# author : Chander
# email : cmsc@gmx.com
# ruby script
require 'uri'
require 'net/http'

# Scriptname variable
scriptname = File.basename(__FILE__)
run_flag = 1

if ARGV.length == 3 
    # convert time period into number of times to probe 
    Total_round = Total_round = ( ARGV[2].to_i * 60 ) / ARGV[1].to_i 
    puts "Needs to check #{Total_round} times"
    puts "Site-Name  " + "        " + "Date/Time" + "                " + "Response-Code" + "      " + "Message"
    1.upto(Total_round) do |i| 
        query = ARGV[0]
        # Inserting http://  into query
        query = "http://" + query
        uri = URI(query) 
        res = Net::HTTP.get_response(uri)
        # Status
        puts ARGV[0] + "  " + Time.now.to_s + "     " + res.code + "  " + res.message 
    	sleep ARGV[1].to_i
    end
else
    puts "Usage: #{scriptname} site_name checking_time_interval(in seconds)  time-period(in Minutes)"
    puts "For Example"
    puts " ruby #{scriptname} gitlab.com  10  5 "
    exit
end